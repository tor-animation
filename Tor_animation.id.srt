1
00:00:00,660 --> 00:00:02,780
Kita sudah terbiasa dengan internet

2
00:00:03,120 --> 00:00:07,700
Kita selalu membagi informasi
mengenai kita dan kehidupan pribadi kita:

3
00:00:08,000 --> 00:00:09,960
makanan yg kita makan, orang2 yang kita temui,

4
00:00:10,180 --> 00:00:12,480
tempat kita berada, dan sesuatu yang kita baca.

5
00:00:13,280 --> 00:00:14,640
Akan saya jelaskan lebih lanjut.

6
00:00:14,920 --> 00:00:17,740
Pada saat ini,
jika seseorang mencoba untuk mencari anda,

7
00:00:18,060 --> 00:00:22,480
mereka akan melihat identitas asli anda,
lokasi, system operasi,

8
00:00:22,800 --> 00:00:26,500
semua halaman web yang sudah anda kunjungi,
applikasi yang anda pakai untuk menjelajahi internet,

9
00:00:26,700 --> 00:00:29,140
dan begitu banyak informasi
mengenai anda dan hidup anda

10
00:00:29,200 --> 00:00:31,500
yang mungkin anda tidak bermaksud
untuk dibagi ke orang asing,

11
00:00:31,700 --> 00:00:34,000
yang bisa dengan mudah menggunakan data2 itu
untuk memanfaatkan anda

12
00:00:34,500 --> 00:00:37,000
Tapi tidak bila anda menggunakan Tor!

13
00:00:37,140 --> 00:00:40,840
Tor Browser melindungi rahasia
dan identitas kita di internet.

14
00:00:41,560 --> 00:00:44,760
Tor mengamankan koneksi anda
dengan 3 lapis pengamanan

15
00:00:44,940 --> 00:00:49,760
dan melewatkannya melalui 3
server yang beroperasi bebas di seluruh dunia

16
00:00:50,280 --> 00:00:53,520
yang mana membantu kita untuk berkomunikasi
tanpa menunjukkan identitas kita di internet.

17
00:00:56,560 --> 00:00:58,280
Tor juga melindungi data kita

18
00:00:58,400 --> 00:01:01,900
dari intaian perusahaan besar atau pemerintah
dan pengawasan massa.

19
00:01:02,880 --> 00:01:07,340
Mungkin anda hidup di negara yg terkekang
yg mencoba untuk mengontrol dan mengawasi internet.

20
00:01:07,900 --> 00:01:11,800
Atau mungkin anda tidak menginginkan perusahaan besar
mengambil keuntungan dari informasi pribadi anda.

21
00:01:12,880 --> 00:01:15,640
Tor membuat semua penggunanya
terlihat sama

22
00:01:15,920 --> 00:01:18,800
yang akan membuat bingung pengawas
and membuat anda tidak terlihat.

23
00:01:19,500 --> 00:01:22,980
Jadi, lebih banyak orang yg memakai jaringan Tor,
akan membuat jaringannya lebih kuat

24
00:01:23,140 --> 00:01:27,800
dan akan membuat lebih mudah untuk bersembunyi di kerumunan
pengguna yang tampak sama persis.

25
00:01:28,700 --> 00:01:31,240
Anda dapat melewati sensor
tanpa harus kuatir mengenai

26
00:01:31,400 --> 00:01:34,100
sensornya akan mengetahui apa yang anda lakukan
di internet.

27
00:01:36,540 --> 00:01:39,440
Iklan-iklannya tidak akan mengikuti anda
kemana-mana selama berbulan-bulan,

28
00:01:39,640 --> 00:01:41,300
mulai dari pertama anda
menekan gambar suatu barang.

29
00:01:43,880 --> 00:01:47,380
Dengan menggunakan Tor, situs-situs yang anda kunjungi
tidak akan tahu mengenai anda,

30
00:01:47,540 --> 00:01:49,760
dari bagian dunia mana anda mengunjungi situs mereka.

31
00:01:49,920 --> 00:01:51,920
kecuali anda memasukkan user dan password anda.

32
00:01:54,200 --> 00:01:55,840
Dengan mengunduh dan menggunakan Tor,

33
00:01:56,200 --> 00:01:58,560
anda dapat melindungi orang-orang
yang memerlukan privasi,

34
00:01:58,880 --> 00:02:01,640
seperti aktivists, journalists dan blogger.

35
00:02:02,000 --> 00:02:07,000
Unduh dan gunakan Tor! Atau gunakan relaynya!

