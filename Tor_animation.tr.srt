1
00:00:00,660 --> 00:00:02,780
İnternete çok alıştık.

2
00:00:03,120 --> 00:00:07,700
Sürekli kendimiz ve özel hayatımızla ilgili bilgiler paylaşıyoruz.

3
00:00:08,000 --> 00:00:09,960
Yediğimiz yemekler, tanıştığımız insanlar,

4
00:00:10,180 --> 00:00:12,480
gittiğimiz yerler ve okuduklarımız dahil olmak üzere.

5
00:00:13,280 --> 00:00:14,640
Daha iyi açıklayayım.

6
00:00:14,920 --> 00:00:17,740
Şu an, biri sizi aratmaya çalışsa;

7
00:00:18,060 --> 00:00:22,480
sizin gerçek kimliğinizi, detaylı lokasyonunuzu, işletim sisteminizi,

8
00:00:22,800 --> 00:00:26,500
ziyaret ettiğiniz bütün internet sayfalarını, internette gezmek için kullandığınız tarayıcıyı,

9
00:00:26,700 --> 00:00:29,140
hayatınız ve sizinle ilgili daha bir çok bilgiyi görebilir.

10
00:00:29,200 --> 00:00:31,500
Herhangi bir veri paylaşmayı planlamadığınız yabancılar, kişisel bilgilerinize erişebilir

11
00:00:31,700 --> 00:00:34,000
ve bu bilgileri kolaylıkla kötüye kullanabilirler.

12
00:00:34,500 --> 00:00:37,000
Fakat Tor kullanırsanız bunları önlersiniz.

13
00:00:37,140 --> 00:00:40,840
Tor tarayıcısı internetteki gizliliğinizi ve kimliğinizi korur.

14
00:00:41,560 --> 00:00:44,760
Tor, internet bağlantınızı üç aşamalı şifrelemeyle sağlamlaştırır 

15
00:00:44,940 --> 00:00:49,760
ve verileri dünya çapında gönüllülerin çalıştırdığı üç sunucudan geçirir.

16
00:00:50,280 --> 00:00:53,520
Bu işlemler, internette anonim bir şekilde iletişim kurabilmemizi sağlar.

17
00:00:56,560 --> 00:00:58,280
Bunun yanı sıra Tor verilerimizi 

18
00:00:58,400 --> 00:01:01,900
kurum ve devletlerin bireyleri ya da kitleleri hedef alarak gözetmesinden korur.

19
00:01:02,880 --> 00:01:07,340
Belki interneti kontrol etmeye ve gözetlemeye çalışan baskıcı bir ülkede yaşıyorsunuz.

20
00:01:07,900 --> 00:01:11,800
Belki de büyük kurumların kişisel bilgilerinizden faydalanmalarını istemiyorsunuz.

21
00:01:12,880 --> 00:01:15,640
Tor bütün kullanıcılarının aynı görünmesini sağlayarak

22
00:01:15,920 --> 00:01:18,800
gözetleyenlerin kafasını karıştırır ve sizi anonim kılar.

23
00:01:19,500 --> 00:01:22,980
Tor ağını ne kadar fazla insan kullanırsa, ağ o kadar güçlenir,

24
00:01:23,140 --> 00:01:27,800
çünkü bireyleri tamamen birbirine benzeyen bir kalabalık içerisinde saklanmak daha kolaydır.

25
00:01:28,700 --> 00:01:31,240
Denetimi,

26
00:01:31,400 --> 00:01:34,100
denetleyicilerin internette ne yaptığınızı bilmesinden kaygılanmadan atlatabilirsiniz.

27
00:01:36,540 --> 00:01:39,440
Artık reklamlar, bir ürüne tıkladığınız andan itibaren, sizi

28
00:01:39,640 --> 00:01:41,300
aylarca takip etmezler.

29
00:01:43,880 --> 00:01:47,380
Tor kullanırsanız, ziyaret ettiğiniz sayfalar kim olduğunuzu,

30
00:01:47,540 --> 00:01:49,760
dünyanın neresinde olduğunuzu;

31
00:01:49,920 --> 00:01:51,920
oturum açıp, yerinizi belirtmediğiniz sürece bilemezler.

32
00:01:54,200 --> 00:01:55,840
Tor'u indirip kullanarak

33
00:01:56,200 --> 00:01:58,560
aktivist, gazeteci ve blogcular gibi

34
00:01:58,880 --> 00:02:01,640
anonimliğe ihtiyacı olan insanları koruyabilirsiniz.

35
00:02:03,000 --> 00:02:07,000
Tor'u indirin ve kullanın! Ya da ara sunucu çalıştırın!

36
00:02:12,400 --> 00:02:15,200
Aylin Caliskan-Islam Tercüme

