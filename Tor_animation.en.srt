1
00:00:00,660 --> 00:00:02,780
We've gotten very used to the Internet.

2
00:00:03,120 --> 00:00:07,700
We are constantly sharing information
about ourselves and our private lives:

3
00:00:08,000 --> 00:00:09,960
food we eat, people we meet,

4
00:00:10,180 --> 00:00:12,480
places we go, and the stuff we read.

5
00:00:13,280 --> 00:00:14,640
Let me explain it better.

6
00:00:14,920 --> 00:00:17,740
Right at this moment,
if someone attempts to look you up,

7
00:00:18,060 --> 00:00:22,480
they'll see your real identity,
precise location, operating system,

8
00:00:22,800 --> 00:00:26,500
all the sites you've visited,
the browser you use to surf the web,

9
00:00:26,700 --> 00:00:29,140
and so much more information
about you and your life

10
00:00:29,200 --> 00:00:31,500
which you probably didn't mean
to share with unknown strangers,

11
00:00:31,700 --> 00:00:34,000
who could easily use this data
to exploit you.

12
00:00:34,500 --> 00:00:37,000
But not if you're using Tor!

13
00:00:37,140 --> 00:00:40,840
Tor Browser protects our privacy
and identity on the Internet.

14
00:00:41,560 --> 00:00:44,760
Tor secures your connection
with three layers of encryption

15
00:00:44,940 --> 00:00:49,760
and passes it through three voluntarily
operated servers around the world,

16
00:00:50,280 --> 00:00:53,520
which enables us to communicate
anonymously over the Internet.

17
00:00:56,560 --> 00:00:58,280
Tor also protects our data

18
00:00:58,400 --> 00:01:01,900
against corporate or government targeted
and mass surveillance.

19
00:01:02,880 --> 00:01:07,340
Perhaps you live in a repressive country
which tries to control and surveil the Internet.

20
00:01:07,900 --> 00:01:11,800
Or perhaps you don't want big corporations
taking advantage of your personal information.

21
00:01:12,880 --> 00:01:15,640
Tor makes all of its users
to look the same

22
00:01:15,920 --> 00:01:18,800
which confuses the observer
and makes you anonymous.

23
00:01:19,500 --> 00:01:22,980
So, the more people use the Tor network,
the stronger it gets

24
00:01:23,140 --> 00:01:27,800
as it's easier to hide in a crowd
of people who look exactly the same.

25
00:01:28,700 --> 00:01:31,240
You can bypass the censorship
without being worried about

26
00:01:31,400 --> 00:01:34,100
the censor knowing what you do
on the Internet.

27
00:01:36,540 --> 00:01:39,440
The ads won't follow you
everywhere for months,

28
00:01:39,640 --> 00:01:41,300
starting when you first
clicked on a product.

29
00:01:43,880 --> 00:01:47,380
By using Tor, the sites you visit
won't even know who you are,

30
00:01:47,540 --> 00:01:49,760
from what part of the world
you're visiting them,

31
00:01:49,920 --> 00:01:51,920
unless you login and tell them so.

32
00:01:54,200 --> 00:01:55,840
By downloading and using Tor,

33
00:01:56,200 --> 00:01:58,560
you can protect the people
who need anonymity,

34
00:01:58,880 --> 00:02:01,640
like activists, journalists and bloggers.

35
00:02:02,000 --> 00:02:07,000
Download and use Tor! Or run a relay!

