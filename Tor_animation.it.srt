1
00:00:00,660 --> 00:00:02,780
Facciamo un uso intenso di Internet

2
00:00:03,120 --> 00:00:07,700
Condividiamo constantemente informazioni
riguardo noi stessi e le nostre vite private:

3
00:00:08,000 --> 00:00:09,960
il cibo che mangiamo, le persone che incontriamo,

4
00:00:10,180 --> 00:00:12,480
i posti dove andiamo, quello che leggiamo.

5
00:00:13,280 --> 00:00:14,640
Permettimi di spiegare meglio.

6
00:00:14,920 --> 00:00:17,740
Proprio in questo momento,
se qualcuno tenta di controllarti,

7
00:00:18,060 --> 00:00:22,480
vedrà la tua vera identità
la tua posizione effettiva, il sistema operativo che usi,

8
00:00:22,800 --> 00:00:26,500
tutti i siti che hai visitato,
il browser che usi per navigare,

9
00:00:26,700 --> 00:00:29,140
e tante altre informazioni
riguardo te e la tua vita,

10
00:00:29,200 --> 00:00:31,500
le quali probabilmente non intendi
condividere con persone sconosciute,

11
00:00:31,700 --> 00:00:34,000
le quali possono facilmente utilizzare questi dati,
per sfruttarti.

12
00:00:34,500 --> 00:00:37,000
Ma non se usi Tor!

13
00:00:37,140 --> 00:00:40,840
Tor Browser protegge la tua privacy
e la tua identità all'interno di Internet.

14
00:00:41,560 --> 00:00:44,760
Tor mette in sicurezza la tua connessione
con tre livelli di criptazione

15
00:00:44,940 --> 00:00:49,760
passando attraverso tre server
volontariamente operativi in giro per il mondo,

16
00:00:50,280 --> 00:00:53,520
i quali ci abilitano a comunicare
anonimamente in Internet.

17
00:00:56,560 --> 00:00:58,280
Tor inoltre protegge i nostri dati

18
00:00:58,400 --> 00:01:01,900
contro determinati enti o governi
e sorveglianza di massa.

19
00:01:02,880 --> 00:01:07,340
Forse vivi  in un paese repressivo
il quale prova a controllare e sorvegliare Internet.

20
00:01:07,900 --> 00:01:11,800
O forse non vuoi che grandi aziende
prendano le tue informazioni personali a loro vantaggio.

21
00:01:12,880 --> 00:01:15,640
Tor fa in modo che tutti i suoi utenti
appaiano uguali

22
00:01:15,920 --> 00:01:18,800
confondendo l'osservatore
e rendendoti anonimo.

23
00:01:19,500 --> 00:01:22,980
Per cui, più persone utilizzano la rete Tor,
più forte diventa quest'ultima

24
00:01:23,140 --> 00:01:27,800
così come è semplice nascondersi in un gruppo
di persone che sembrano esattamente le stesse.

25
00:01:28,700 --> 00:01:31,240
Puoi aggirare la censura
senza alcuna preoccupazione

26
00:01:31,400 --> 00:01:34,100
riguardo ciò che il censore sa dei tuoi
movimenti su internet.

27
00:01:36,540 --> 00:01:39,440
Le pubblicità non ti seguiranno
ovunque per mesi,

28
00:01:39,640 --> 00:01:41,300
quando clicchi per la prima volta
su un prodotto.

29
00:01:43,880 --> 00:01:47,380
Usando Tor, i siti che tu visiti
non sapranno nemmeno chi tu sia,

30
00:01:47,540 --> 00:01:49,760
da quale parte del mondo
li stai visitando,

31
00:01:49,920 --> 00:01:51,920
a meno che tu non lo dica a loro facendo il login.

32
00:01:54,200 --> 00:01:55,840
Scaricando ed utilizzando Tor,

33
00:01:56,200 --> 00:01:58,560
tu puoi proteggere le persone
che hanno bisogno di anonimato,

34
00:01:58,880 --> 00:02:01,640
come attivisti, giornalisti e blogger.

35
00:02:02,000 --> 00:02:07,000
Scarica e utilizza Tor! O installa un relay!

36
00:02:12,400 --> 00:02:15,200
Traduzione a cura di Antoci Rosario

