1
00:00:00,660 --> 00:00:02,780
Vi har blivit väldigt vana med internet.

2
00:00:03,120 --> 00:00:07,700
Vi delar ständigt med oss information
om oss själva och våra privatliv.

3
00:00:08,000 --> 00:00:09,960
Vilken mat vi äter, vilka vi träffar,

4
00:00:10,180 --> 00:00:12,480
platser vi besöker och vad vi läser.

5
00:00:13,280 --> 00:00:14,640
Låt mig förklara lite utförligare.

6
00:00:14,920 --> 00:00:17,740
Just nu, om någon försöker kolla dig

7
00:00:18,060 --> 00:00:22,480
så kommer de hitta din identitet,
din exakta position, ditt operativsystem,

8
00:00:22,800 --> 00:00:26,500
alla sajter du besökt,
vilken webbläsare du använder

9
00:00:26,700 --> 00:00:29,140
och mycket mer information
om dig och ditt liv

10
00:00:29,200 --> 00:00:31,500
som du antagligen inte hade tänkt
dela med okända främlingar

11
00:00:31,700 --> 00:00:34,000
som enkelt kan använda detta
för att utnyttja dig.

12
00:00:34,500 --> 00:00:37,000
Men inte om du använder Tor!

13
00:00:37,140 --> 00:00:40,840
Tor-webbläsaren skyddar ditt privatliv
och din identitet på internet.

14
00:00:41,560 --> 00:00:44,760
Tor säkrar din kommunikation
med tre lager kryptering

15
00:00:44,940 --> 00:00:49,760
och skickar den genom tre frivilligt
skötta servrar runt om i världen

16
00:00:50,280 --> 00:00:53,520
som gör det möjligt att kommunicera
anonymt på internet.

17
00:00:56,560 --> 00:00:58,280
Tor skyddar också dina uppgifter

18
00:00:58,400 --> 00:01:01,900
från företag eller myndigheter
och från massövervakning.

19
00:01:02,880 --> 00:01:07,340
Kanske bor du i en repressiv stat som
försöker kontrollera och övervaka internet

20
00:01:07,900 --> 00:01:11,800
eller så vill du inte att stora företag
ska utnyttja dina personuppgifter.

21
00:01:12,880 --> 00:01:15,640
Tor gör så att alla dess användare
ser likadana ut

22
00:01:15,920 --> 00:01:18,800
vilket förvirrar övervakaren
och gör dig anonym.

23
00:01:19,500 --> 00:01:22,980
Ju fler som använder Tor-nätverket
desto starkare blir det

24
00:01:23,140 --> 00:01:27,800
eftersom det är enklare att gömma sig
i en grupp där alla ser exakt likadana ut.

25
00:01:28,700 --> 00:01:31,240
Du kan komma runt censuren
utan att oroa dig för

26
00:01:31,400 --> 00:01:34,100
att övervakaren vet vad du gör
på internet.

27
00:01:36,540 --> 00:01:39,440
Annonser kommer inte
förfölja dig i flera månader

28
00:01:39,640 --> 00:01:41,300
efter att du har klickat
på en produkt.

29
00:01:43,880 --> 00:01:47,380
Om du använder Tor kommer inte ens
sajterna du besöker veta vem du är

30
00:01:47,540 --> 00:01:49,760
och från vilken del av världen
du kommer

31
00:01:49,920 --> 00:01:51,920
om du inte loggar in och berättar det.

32
00:01:54,200 --> 00:01:55,840
Genom att ladda ner och använda Tor

33
00:01:56,200 --> 00:01:58,560
kan du skydda de personer
som behöver vara anonyma

34
00:01:58,880 --> 00:02:01,640
som aktivister, journalister och bloggare.

35
00:02:02,000 --> 00:02:07,000
Ladda ner och använd Tor!
Eller starta ett relä!

