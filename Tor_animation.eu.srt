1
00:00:02,000 --> 00:00:04,500
Erabat ohitu gara Internet erabiltzera.

2
00:00:04,700 --> 00:00:07,700
Gure bizitzako informazio pribatua
sareratzen dugu etengabe:

3
00:00:08,000 --> 00:00:11,000
zer jaten dugun,
nor bisitatzen dugun,

4
00:00:11,200 --> 00:00:14,200
nora goazen
eta zer irakurtzen dugun.

5
00:00:14,500 --> 00:00:16,400
Utzidazu hobeto azaltzen.

6
00:00:16,600 --> 00:00:19,600
Oraintxe bertan,
aztertu nahi bazindute,

7
00:00:19,800 --> 00:00:23,200
zure benetako nortasuna,
kokapen zehatza, sistema eragilea,

8
00:00:23,400 --> 00:00:26,500
ikusitako webgune oro,
sarean erabilitako nabigatzailea,

9
00:00:26,700 --> 00:00:29,000
eta askoz informazio gehiago ikus dezakete
zutaz eta zeure bizitzaz

10
00:00:29,200 --> 00:00:31,500
ziur aski ezezagunekin
banatu nahi ez duzuna,

11
00:00:31,700 --> 00:00:34,000
erraztazunez zu kaltetzeko
erabil dezaketen horiekin.

12
00:00:34,500 --> 00:00:37,000
Baina ez Tor erabiltzen baduzu!

13
00:00:37,200 --> 00:00:40,800
Tor Browser-ek zure pribatutasuna
eta nortasuna babesten ditu Interneten.

14
00:00:41,000 --> 00:00:44,800
Tor-ek zure konexioa babesten du
hiru zifraketa-geruza erabiliz

15
00:00:45,000 --> 00:00:49,300
eta munduan zehar bolondreski kudeatutako
hiru zerbitzari desberdinetatik bideratzen du,

16
00:00:49,500 --> 00:00:54,500
Interneten zehar anonimoki
komunikatzea ahalbidetuz.

17
00:00:55,000 --> 00:00:57,400
Tor-ek gainera gure datuak babesten ditu

18
00:00:57,600 --> 00:01:02,600
gobernu zein enpresen espioitza selektibo eta masibotik.

19
00:01:02,800 --> 00:01:07,800
Baliteke zu, Internet kontrolatzen eta zelatatzen duen herrialde errepresibo baten bizitzea.

20
00:01:08,000 --> 00:01:13,000
Edo beharbada ez duzu nahi konpainia handiak zure informazio pertsonalaz baliatzea.

21
00:01:13,800 --> 00:01:16,500
Tor-ek, bere erabiltzaile guztiak
berdintsu egiten ditu

22
00:01:16,700 --> 00:01:20,800
zelataria nahasiz eta zu anonimo bihurtuz.

23
00:01:21,900 --> 00:01:25,700
Hortaz, zenbat eta jende gehiagok Tor sarea erabili, orduan eta indartsuago bilakatuko da,

24
00:01:25,800 --> 00:01:29,800
ezkutatzea errazago bait da itxura berbera daukan jendetzaren barnean.

25
00:01:30,000 --> 00:01:32,500
Zentsura saihestu dezakezu
Interneten egiten duzunaz

26
00:01:32,700 --> 00:01:36,100
zelatariak jakin dezakenari buruz
kezkatu beharrik gabe

27
00:01:36,300 --> 00:01:39,200
Iragarkiek jada ez dizute
hilabeteetako jarraipenik egingo

28
00:01:39,400 --> 00:01:43,300
zoazen leku guztietara
produktu bat lehen aldiz klikatu ostean.

29
00:01:43,800 --> 00:01:46,300
Tor erabiliz, bisitatzen dituzun guneek
ez dute jakingo nor zaren

30
00:01:46,500 --> 00:01:48,500
ezta munduko zein lekutik
bisitatzen dituzun,

31
00:01:48,700 --> 00:01:50,700
identifikatu eta zuk zeuk esan ezean.

32
00:01:53,200 --> 00:01:55,500
Tor jaitsi eta erabiliz,

33
00:01:55,700 --> 00:01:58,000
anonimotasuna behar duten pertsonak babestuko dituzu,

34
00:01:58,200 --> 00:02:03,500
esate baterako: Aktibistak, kazetariak eta blogariak.

35
00:02:04,000 --> 00:02:09,000
Jaitsi eta erabili Tor! Edo jarri martxan Tor errepikagailua!

36
00:02:12,400 --> 00:02:15,200
Hack.in#badakigu -k Itzulita
