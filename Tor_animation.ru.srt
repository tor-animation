1
00:00:00,000 --> 00:00:03,010
Мы сильно привыкли к интернету.

2
00:00:03,010 --> 00:00:07,750
Всё время делимся информацией
о себе и нашей личной жизни.

3
00:00:07,750 --> 00:00:09,520
что едим, с кем встречаемся,

4
00:00:09,520 --> 00:00:12,800
куда ходим, что читаем.

5
00:00:12,805 --> 00:00:14,525
Объясняю точнее.

6
00:00:14,525 --> 00:00:18,525
Прямо сейчас, если кто-то
попробует найти вас,

7
00:00:18,525 --> 00:00:21,345
он опознает вас, где вы и
вашу операционную систему,

8
00:00:21,345 --> 00:00:24,465
на каких сайтах были,
каким браузером пользуетесь,

9
00:00:24,465 --> 00:00:27,745
и ещё уйму информации о вас и вашей жизни,

10
00:00:27,745 --> 00:00:31,075
чем вы вряд ли хотели бы
поделится с незнакомцами,

11
00:00:31,075 --> 00:00:34,405
ведь они могут скомпрометировать вас.

12
00:00:34,695 --> 00:00:36,915
Но ТОР предотвратит это!

13
00:00:36,915 --> 00:00:41,845
Браузер ТОРа защищает конфиденциальность
и нашу личность в интернете.

14
00:00:41,845 --> 00:00:45,725
ТОР обеспечивает трёхслойную кодировку

15
00:00:45,725 --> 00:00:49,725
пропуская сигнал через три добровольно
работающих сервера по всему свету,

16
00:00:49,725 --> 00:00:53,725
что позволяет нам общаться
анонимно по интернету.

17
00:00:55,056 --> 00:00:59,056
ТОР также охраняет нашу информацию

18
00:00:59,056 --> 00:01:02,946
от любых форм надзора, будь то
корпоративного или правительственного.

19
00:01:02,946 --> 00:01:06,568
Предположим вы живёте при репрессивном
строе, где интернет перлюстрируется.

20
00:01:06,568 --> 00:01:10,190
Или вы не хотите, чтобы какая-то корпорация

21
00:01:10,190 --> 00:01:13,812
воспользовалась вашими личными данными.

22
00:01:13,939 --> 00:01:17,939
Благодаря ТОРу все
пользователи выглядят одинаково

23
00:01:17,939 --> 00:01:21,939
это сбивает постороннего с толку
и гарантирует вашу анонимность.

24
00:01:21,939 --> 00:01:25,939
Чем больше людей используют ТОР,
тем мощнее он становится

25
00:01:25,939 --> 00:01:28,909
ведь куда проще скрыться в толпе,
где все выглядят одинаково.

26
00:01:30,039 --> 00:01:34,039
Вы можете обойти цензуру,
не беспокоясь о том,

27
00:01:34,039 --> 00:01:37,159
знает ли цензор, что вы делаете
в интернете.

28
00:01:37,159 --> 00:01:41,159
реклама не станет вашим “хвостом”
на долгие месяцы

29
00:01:41,159 --> 00:01:44,069
потому что вы однажды щёлкнули на товар.

30
00:01:44,069 --> 00:01:48,069
При режиме ТОР сайты, которые вы посещаете
не будут даже знать, кто вы,

31
00:01:48,069 --> 00:01:50,429
из какой вы страны,

32
00:01:50,429 --> 00:01:54,429
разве что вы сами делаете вход в систему
и оповещаете о себе.

33
00:01:54,429 --> 00:01:57,029
Загрузив и используя ТОР,

34
00:01:57,029 --> 00:02:01,079
вы защитите тех, кто
нуждается в анонимности,

35
00:02:01,079 --> 00:02:05,079
скажем, активистов,
журналистов и блоггеров.

36
00:02:05,079 --> 00:02:11,059
Загрузите и пользуйтесь ТОРом!
Или запустите ретрансляторы!

37
00:02:12,400 --> 00:02:15,200
Перевод Дирана Мегребляна

