1
00:00:00,660 --> 00:00:02,780
Olemme tottuneet käyttämään Internetiä.

2
00:00:03,120 --> 00:00:07,700
Jaamme koko ajan tietoa
itsestämme ja elämästämme:

3
00:00:08,000 --> 00:00:09,960
aterioistamme, tuttavistamme,

4
00:00:10,180 --> 00:00:12,480
matkoistamme ja lukutottumuksistamme.

5
00:00:13,280 --> 00:00:14,640
Selitän tarkemmin.

6
00:00:14,920 --> 00:00:17,740
Jos joku yrittää juuri nyt
selvittää kuka olet,

7
00:00:18,060 --> 00:00:22,480
he tietävät pian henkilöllisyytesi,
sijaintisi, käyttöjärjestelmäsi,

8
00:00:22,800 --> 00:00:26,500
kaikki sivustot, joilla olet vieraillut,
käyttämäsi selaimen

9
00:00:26,700 --> 00:00:29,140
ja lisäksi paljon muuta tietoa
sinusta ja elämästäsi,

10
00:00:29,200 --> 00:00:31,500
jota et luultavasti halunnut
jakaa tuntemattomien kanssa,

11
00:00:31,700 --> 00:00:34,000
jotka voivat helposti käyttää tietojasi
sinua vastaan.

12
00:00:34,500 --> 00:00:37,000
Se ei ole mahdollista,
jos käytät Tor-selainta.

13
00:00:37,140 --> 00:00:40,840
Tor-selain suojelee yksityisyyttäsi
ja henkilöllisyyttäsi Internetissä.

14
00:00:41,560 --> 00:00:44,760
Tor varmistaa yhteytesi
kolminkertaisella salauksella

15
00:00:44,940 --> 00:00:49,760
ja välittää sen kolmen eri puolilla maailmaa
sijaitsevan vapaaehtoisen palvelimen kautta,

16
00:00:50,280 --> 00:00:53,520
mikä mahdollistaa anonyymin
kommunikoinnin Internetissä.

17
00:00:56,560 --> 00:00:58,280
Tor myös suojelee tietojasi

18
00:00:58,400 --> 00:01:01,900
yritysmaailmalta, viranomaisilta
ja massaurkinnalta.

19
00:01:02,880 --> 00:01:07,340
Voi olla, että asut maassa,
joka yrittää tarkkailla ja hallita verkkoa.

20
00:01:07,900 --> 00:01:11,800
Tai ehkä et halua suuryritysten
käyttävän hyväksi henkilökohtaisia tietojasi.

21
00:01:12,880 --> 00:01:15,640
Tor tekee kaikista käyttäjistään samannäköisiä,

22
00:01:15,920 --> 00:01:18,800
mikä hämmentää tarkkailijoita
ja tekee kaikista anonyymejä.

23
00:01:19,500 --> 00:01:22,980
Eli mitä useampi henkilö käyttää Tor-verkkoa,
sitä vahvempi verkko on,

24
00:01:23,140 --> 00:01:27,800
koska on helpompi piiloutua väkijoukkoon,
jossa kaikki näyttävät samalta.

25
00:01:28,700 --> 00:01:31,240
Voit ohittaa sensuurin ilman,
että sinun tarvitsee välittää sensorista

26
00:01:31,400 --> 00:01:34,100
tai siitä, että hän tietää
mitä teet Internetissä.

27
00:01:36,540 --> 00:01:39,440
Mainokset eivät seuraa
sinua kuukausikaupalla,

28
00:01:39,640 --> 00:01:41,300
jos klikkaat jotain tuotetta.

29
00:01:43,880 --> 00:01:47,380
Käyttämällä Toria, sivustot
eivät edes tiedä kuka olet

30
00:01:47,540 --> 00:01:49,760
tai mistä päin maailmaa
päädyit heidän sivuilleen,

31
00:01:49,920 --> 00:01:51,920
paitsi jos kirjaudut ja kerrot heille.

32
00:01:54,200 --> 00:01:55,840
Lataamalla ja käyttämällä Tor-selainta

33
00:01:56,200 --> 00:01:58,560
voit myös suojella ihmisiä,
jotka tarvitsevat anonymiteettiä.

34
00:01:58,880 --> 00:02:01,640
Kuten aktivisteja, toimittajia ja bloggaajia.

35
00:02:02,000 --> 00:02:07,000
Lataa Tor ja käytä sitä! Tai ylläpidä Tor-solmua!

36
00:02:12,400 --> 00:02:15,200
Kääntäjä Tommi Kakko

