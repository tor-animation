1
00:00:00,660 --> 00:00:02,780
Mēs esam ļoti pieraduši pie interneta.

2
00:00:03,120 --> 00:00:07,700
Mēs nepārtraukti dalāmies ar informāciju
par sevi un savu privāto dzīvi:

3
00:00:08,000 --> 00:00:09,960
ēdienu, ko ēdam, cilvēkiem, kurus satiekam,

4
00:00:10,180 --> 00:00:12,480
vietām, uz kurām dodamies, un arī to, ko lasām.

5
00:00:13,280 --> 00:00:14,640
Ļaujiet man paskaidrot labāk.

6
00:00:14,920 --> 00:00:17,740
Tieši šobrīd,
ja kāds centīsies Jūs atrast,

7
00:00:18,060 --> 00:00:22,480
šī persona noskaidros Jūsu patiesu identitāti,
precīzu atrašanās vietu, operētājsistēmu,

8
00:00:22,800 --> 00:00:26,500
visas Jūsu apmeklētās tīmekļa vietnes,
tīmekļa pārlūkošanai izmantoto pārlūku,

9
00:00:26,700 --> 00:00:29,140
un tik daudz vairāk informācijas
par Jums un Jūsu dzīvi

10
00:00:29,200 --> 00:00:31,500
cik, ticami, Jūs nebijāt domājis
kopīgot ar nezināmiem svešiniekiem,

11
00:00:31,700 --> 00:00:34,000
kas var viegli izmantot šos datus
pret Jums.

12
00:00:34,500 --> 00:00:37,000
Taču ne tad, ja izmantojat Tor!

13
00:00:37,140 --> 00:00:40,840
Tor pārlūks aizsargā mūsu privātumu
 un identitāti internetā.

14
00:00:41,560 --> 00:00:44,760
Tor nodrošina jūsu savienojumu
ar trīs dažādiem šifriem

15
00:00:44,940 --> 00:00:49,760
un izlaiž cauri trīs brīvprātīgi
strādājošiem serveriem pasaulē,

16
00:00:50,280 --> 00:00:53,520
kas atļauj mums komunicēt
internetā anonīmi.

17
00:00:56,560 --> 00:00:58,280
Tor arī aizsargā mūsu datus

18
00:00:58,400 --> 00:01:01,900
pret korporatīvo vai valdības mērķēto
un masu uzraudzību.

19
00:01:02,880 --> 00:01:07,340
Iespējams, jūs dzīvojat represīvā valstī,
kas cenšas konrolēt un uzraudzīt internetu.

20
00:01:07,900 --> 00:01:11,800
Vai arī iespējams, ka jūs nevēlaties, lai lielas korporācijas
gūtu labumu no jūsu personīgās informācijas.

21
00:01:12,880 --> 00:01:15,640
Tor padara visus tā lietotājus
izskatīties vienādiem,

22
00:01:15,920 --> 00:01:18,800
kas apmulsina novērotāju
un padara Jūs anonīmus.

23
00:01:19,500 --> 00:01:22,980
Tātad, jo vairāk cilvēku lieto Tor tīklu,
jo stiprāks tas kļūst

24
00:01:23,140 --> 00:01:27,800
un ir vieglāk paslēpties pūlī,
kur visi cilvēki izskatās pilnīgi vienādi.

25
00:01:28,700 --> 00:01:31,240
Jūs variet izvairīties no cenzūras
bez uztraukšanās par

26
00:01:31,400 --> 00:01:34,100
cenzora zināšanu, ko Jūs darāt
internetā.

27
00:01:36,540 --> 00:01:39,440
Reklāmas Jums visur nesekos
mēnešiem ilgi,

28
00:01:39,640 --> 00:01:41,300
sākot no brīža, kad Jūs pirmoreiz 
uzklikšķinājāt uz produktu.

29
00:01:43,880 --> 00:01:47,380
Lietojot Tor, mājaslapas, kuras Jūs apmeklējat
pat nezina, kas Jūs esat,

30
00:01:47,540 --> 00:01:49,760
no kuras pasaules daļas
Jūs to apmeklējat,

31
00:01:49,920 --> 00:01:51,920
ja vien Jūs nepierakstīsities un par to nepastāstīsiet.

32
00:01:54,200 --> 00:01:55,840
Lejupielādējot un lietojot Tor,

33
00:01:56,200 --> 00:01:58,560
Jūs varat aizsargāt cilvēkus,
kuriem anonimitāte ir nepieciešama,

34
00:01:58,880 --> 00:02:01,640
kā piemēram, žurnālistus un blogerus.

35
00:02:02,000 --> 00:02:07,000
Lejuplādējiet un izmantojiet Tor! Vai darbiniet retranslatoru!

