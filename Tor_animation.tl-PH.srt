1
00:00:00,660 --> 00:00:02,780
Sobrang nasanay na tayo sa Internet

2
00:00:03,120 --> 00:00:07,700
Palagi tayong namamahagi ng tungkol sa atin at pati na rin sa ating pribadong buhay:

3
00:00:08,000 --> 00:00:09,960
mga kinakain natin, mga taong kasalamuha natin,

4
00:00:10,180 --> 00:00:12,480
mga lugar na pinupuntahan natin, at mga binabasa natin.

5
00:00:13,280 --> 00:00:14,640
Hayaan mo akong ipaliwanag ito sa iyo ng mas mabuti.

6
00:00:14,920 --> 00:00:17,740
Sa mismong oras na ito,
kapag mayroong sinuman na magtangkang tignan kung sino ka sa Internet,

7
00:00:18,060 --> 00:00:22,480
malalaman nila kung sino ka talaga,
kung nasaan ka mismo, operating system,

8
00:00:22,800 --> 00:00:26,500
lahat ng website na binisita mo,
ang browser na ginagamit mo para mag surf sa web,

9
00:00:26,700 --> 00:00:29,140
at kung ano-ano pa tungkol sa iyo at sobra-sobra pa tungkol sa iyo.

10
00:00:29,200 --> 00:00:31,500
na hindi mo naman talagang sadyang ibahagi sa mga taong hindi mo kilala

11
00:00:31,700 --> 00:00:34,000
- na maaring gamitin ang impormasyon mo na kanila'y pakikinabangan.

12
00:00:34,500 --> 00:00:37,000
Pero lahat ng iyon ay maiiwasan sa pamamagitan ng Tor!

13
00:00:37,140 --> 00:00:40,840
Pinoprotektahan ng Tor Browser ang ating palihim (privacy) at pagkakakilanlan (identity) sa Internet.

14
00:00:41,560 --> 00:00:44,760
Pinapatibay at sinisigurado ng Tor ang iyong koneksyon sa pamamagitan ng tatlong patong ng encryption

15
00:00:44,940 --> 00:00:49,760
at idinadaan ito sa tatlong kinusang-loob na mga servers sa iba't-ibang sulok ng mundo,

16
00:00:50,280 --> 00:00:53,520
para magkaroon tayong nang paliham na komunikasyon sa Internet.

17
00:00:56,560 --> 00:00:58,280
Pinoprotektahan din ng Tor ang ating mga data

18
00:00:58,400 --> 00:01:01,900
laban sa patamaang-gobyerno (government-targeted) o patamaang-korporasyon (corporate-targeted) at pang-masang pamamahala (mass surveillance).

19
00:01:02,880 --> 00:01:07,340
Baka nakatira ka sa isang bansa na strikto sa kalayaan na sinusubukang kontrolin at imahala (surveil) ang Internet.

20
00:01:07,900 --> 00:01:11,800
O kaya nama'y ayaw mong pakinabangan ng malalaking korporasyon ang iyong personal na impormasyon.

21
00:01:12,880 --> 00:01:15,640
Ginagawa ng Tor na magkakamukha ang mga gumagamit nito

22
00:01:15,920 --> 00:01:18,800
na nililito ang sinumang namamahala o nanonood, at sa pamamagitan nito ikaw ay nagiging anonimo o hindi nakikilala.

23
00:01:19,500 --> 00:01:22,980
Kaya, kapag mas maraming taong gumagamit ng Tor network, mas lumalakas ito

24
00:01:23,140 --> 00:01:27,800
sapagkat mas madaling magtago sa pangkat o grupo ng tao na magkakamukha.

25
00:01:28,700 --> 00:01:31,240
Maaring mong iwasan o sikutan ang pagsensura ng hindi pagaalala

26
00:01:31,400 --> 00:01:34,100
tungkol sa pagaalam ng sensura kung ano ang ginagawa mo sa Internet.

27
00:01:36,540 --> 00:01:39,440
Hindi ka susundan ng mga ads kahit saan ng ilang buwan,

28
00:01:39,640 --> 00:01:41,300
simula nung una kang pumindot sa isang produkto.

29
00:01:43,880 --> 00:01:47,380
Sa pagamit ng Tor, hindi ka man lang malaman ng mga sites na binisita mo kung sino ka,

30
00:01:47,540 --> 00:01:49,760
kung saang parte ng mundo mo sila binisita,

31
00:01:49,920 --> 00:01:51,920
kung hindi ka mag lologin at magkukusang magsabi.

32
00:01:54,200 --> 00:01:55,840
Sa pag-download at paggamit ng Tor,

33
00:01:56,200 --> 00:01:58,560
maproprotektahan mo din ang pagiging anonimo ng mga nangangailangang tao,

34
00:01:58,880 --> 00:02:01,640
tulad ng activists, journalists at bloggers.

35
00:02:02,000 --> 00:02:07,000
Kaya'y i-download mo na ang Tor at gamitin! O kaya'y magpatakbo ng relay!

36
00:02:12,400 --> 00:02:15,200
Isinalin (translated) ni anonymous contributor

