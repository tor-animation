1
00:00:00,660 --> 00:00:02,780
Ne jemi mësuar shumë me internetin

2
00:00:03,120 --> 00:00:07,700
Ne jemi në menyrë konstante duke shkëmbyer informacion
për veten tonë dhe jetën tonë private:

3
00:00:08,000 --> 00:00:09,960
ushqimin që hamë, njerëzit që takojmë,

4
00:00:10,180 --> 00:00:12,480
vendet që vizitojmë, dhe gjërat që lexojmë.

5
00:00:13,280 --> 00:00:14,640
Më lejoni ta shpjegoj më mirë.

6
00:00:14,920 --> 00:00:17,740
Tani, në këtë moment,
nëse dikush tenton të ju shikojë,

7
00:00:18,060 --> 00:00:22,480
do munnd të shohin identitetin tuaj të vërtetë,
vendodhjen e saktë, sistemin operativ,

8
00:00:22,800 --> 00:00:26,500
të gjitha faqet që ju keni vizituar,
shfletuesit që ju përdorni për të shfletuar në web,

9
00:00:26,700 --> 00:00:29,140
dhe shumë më shumë informacion
për ju dhe jetën tuaj

10
00:00:29,200 --> 00:00:31,500
të cilën ju ndoshta nuk do donit
ta ndanit me të huajt e të panjohur,

11
00:00:31,700 --> 00:00:34,000
të cilët lehtësisht mund ti përdorin këto të dhëna
për të të shfrytëzuar ty.

12
00:00:34,500 --> 00:00:37,000
Por jo, në qoftë se ju jeni duke përdorur Tor!

13
00:00:37,140 --> 00:00:40,840
Tor Browser mbron privatësinë tonë
dhe identitetin në internet.

14
00:00:41,560 --> 00:00:44,760
Tor siguron lidhjen tuaj
me tri shtresa enkriptimi

15
00:00:44,940 --> 00:00:49,760
dhe kalon nëpërmjet tre serverat
shfrytëzimi në mbarë botën, vullnetarisht,

16
00:00:50,280 --> 00:00:53,520
të cilat na mundësojnë që të komunikojmë
në mënyre anonime në Internet.

17
00:00:56,560 --> 00:00:58,280
Tor gjithashtu mbron të dhënat tona

18
00:00:58,400 --> 00:01:01,900
kundër korporatave apo shënjestërave qeveritare
dhe mbikqyrjes në masë.

19
00:01:02,880 --> 00:01:07,340
Ndoshta ju jetoni në një vend "shtypës"
i cili përpiqet të kontrollojë dhe survejojë internetin.

20
00:01:07,900 --> 00:01:11,800
Ndoshta ju nuk doni që korporatat e mëdha
të përfitojnë nga të dhënat tuaja personale.

21
00:01:12,880 --> 00:01:15,640
Tor bën të gjithë përdoruesit e tij
të duken njëlloj

22
00:01:15,920 --> 00:01:18,800
që të ngatërrojnë vëzhguesit
dhe të ju bëjë juve anonim.

23
00:01:19,500 --> 00:01:22,980
Pra, sa më shumë njerëz të përdorin rrjetin Tor,
aq më i fortë bëhet ai,

24
00:01:23,140 --> 00:01:27,800
është më e lehtë të fshihesh në një turmë
njerëzish që të duken të njëjtë.

25
00:01:28,700 --> 00:01:31,240
Ju mund të anashkaloni censurën
pa qenë u shqetësuar për

26
00:01:31,400 --> 00:01:34,100
censorët, duke ditur atë që ju bëni
në internet.

27
00:01:36,540 --> 00:01:39,440
Reklamat nuk do të ju ndjekin
kudo për muaj me rradhë,

28
00:01:39,640 --> 00:01:41,300
duke filluar, kur ju
klikoni në një produkt për herë të parë.

29
00:01:43,880 --> 00:01:47,380
Duke përdorur Tor, faqet që vizitoni
as nuk do e dinë se kush jeni ju,

30
00:01:47,540 --> 00:01:49,760
nga cila pjesë e botës
ju jeni duke i vizituar,

31
00:01:49,920 --> 00:01:51,920
nëse nuk logoheni dhe u tregoni indentitetin.

32
00:01:54,200 --> 00:01:55,840
Duke shkarkuar dhe përdorur Tor,

33
00:01:56,200 --> 00:01:58,560
ju mbroni njerëzit
të cilët duan anonimitetin,

34
00:01:58,880 --> 00:02:01,640
si aktivistët, gazetarët dhe blogerët.

35
00:02:02,000 --> 00:02:07,000
Shkarkoni dhe përdorni Tor! Ose ofro një shërbim vullnetar.

36
00:02:12,400 --> 00:02:15,200
Përkthimi nga Greta Doçi

